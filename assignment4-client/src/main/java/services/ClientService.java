package services;

import models.Package;
import models.Tracker;
import models.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ClientService {

    List<Package> listPackages(String username);

    List<Package> searchPackages(String username, String packageName);

    List<Tracker>  packageStatus(int id,String username );
}
