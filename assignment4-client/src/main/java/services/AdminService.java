package services;

import models.Package;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.GregorianCalendar;
@WebService
public interface AdminService {

    void addPackage(String senderName,String receiverName,String name,String description,String senderCity,String destinationCity,boolean tracking);

    void deletePackage(int id);

    void registerPackageForTracking(int id);
    void packageStatusUpdating(int id, String location, GregorianCalendar time);
}
