package models;



import java.sql.Timestamp;
import java.util.GregorianCalendar;


public class Tracker {


    private int id;

    private String city;

    private GregorianCalendar time;


    private Package pack;



    public Tracker(String city, GregorianCalendar time,Package pack) {
        this.city = city;
        this.time = time;
        this.pack = pack;
    }
    public Tracker(){

    }
    public int getId() {
        return id;
    }

    public Package getPack() {
        return pack;
    }

    public void setPack(Package pack) {
        this.pack = pack;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public GregorianCalendar getTime() {
        return time;
    }

    public void setTime(GregorianCalendar time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Tracker{" +
                ", city='" + city + '\'' +
                ", time=" + time.getTime() +
                '}'+'\n';
    }
}
