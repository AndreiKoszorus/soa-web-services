package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import models.Package;
import models.Tracker;
import services.ClientService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.util.List;


public class ClientController {
    @FXML
    private TextArea showPackages;
    @FXML
    private TextField packageName;
    @FXML
    private TextArea searchedPackages;
    @FXML
    private TextField packageId;
    @FXML
    private TextArea packageStatus;

    private ClientService clientService;

    private String username;

    public ClientController() throws Exception{
        URL url = new URL("http://localhost:8080/ClientService?wsdl");
        QName qName = new QName("http://sib/","ClientBeanService");
        Service service = Service.create(url,qName);
        qName = new QName("http://sib/","ClientBeanPort");
        this.clientService = service.getPort(qName,ClientService.class);
    }

    public void initData(String username){
        this.username = username;
    }

    public void show(){
        List<Package> packages = clientService.listPackages(username);
        StringBuilder sb = new StringBuilder();
        for(Package pack:packages){
            sb.append(pack.toString());
        }
        showPackages.setText(sb.toString());
    }

    public void searchPackages(){
        List<Package> packages = clientService.searchPackages(username,packageName.getText());
        StringBuilder sb = new StringBuilder();
        for(Package pack:packages){
            sb.append(pack.toString());
        }
        searchedPackages.setText(sb.toString());
    }

    public void showTracks(){
        List<Tracker> tracks = clientService.packageStatus(Integer.parseInt(packageId.getText()),username);
        StringBuilder sb = new StringBuilder();
        for(Tracker track:tracks){
            sb.append(track.toString());
        }
        System.out.println(tracks.size());
        packageStatus.setText(sb.toString());
    }



}
