package controllers;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import services.AdminService;
import services.LoginService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;


public class LoginController extends Application {
    @FXML
    private TextField username;
    @FXML
    private TextField password;
    @FXML
    private Label messageLabel;

    private LoginService loginService;

    private String savedUsername;

    public LoginController() throws MalformedURLException {

        URL url = new URL("http://localhost:8080/LoginService?wsdl");
        QName qName = new QName("http://sib/","LoginBeanService");
        Service service = Service.create(url,qName);
        qName = new QName("http://sib/","LoginBeanPort");
        this.loginService = service.getPort(qName,LoginService.class);
    }

    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        // Path to the FXML File
        String fxmlDocPath = "E://SD//AK//assignment4-client//src//main//java//view//LoginVIew.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        //System.out.println(username);

        // Create the Pane and all Details
        AnchorPane root = (AnchorPane) loader.load(fxmlStream);
        // Create the Scene
        Scene scene = new Scene(root);
        // Set the Scene to the Stage
        primaryStage.setScene(scene);
        // Set the Title to the Stage
        primaryStage.setTitle("Login");
        // Display the Stage
        primaryStage.show();
    }

    public void login() throws Exception{
        int ckeckLogin = loginService.checkLogin(username.getText(),password.getText());
        if(ckeckLogin == 0){
            messageLabel.setText("Success");
            this.savedUsername = username.getText();
            if(loginService.isAdmin(username.getText())){
                showAdminView();
            }
            else{
                showClientView();
            }
        }
        else if(ckeckLogin == 1){
            messageLabel.setText("Invalid password");
        }
        else{
            messageLabel.setText("Username doesn't exists");
        }
    }

    private void showAdminView() throws Exception{
        FXMLLoader loader = new FXMLLoader();
        String fxmlDocPath = "E://SD//AK//assignment4-client//src//main//java//view//AdminView.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        AnchorPane root = (AnchorPane) loader.load(fxmlStream);
        Scene scene = new Scene(root);
        Stage stage=new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(scene);

        stage.show();
    }

    public void showClientView() throws Exception{
        FXMLLoader loader = new FXMLLoader();
        String fxmlDocPath = "E://SD//AK//assignment4-client//src//main//java//view//ClientView.fxml";
        FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
        AnchorPane root = (AnchorPane) loader.load(fxmlStream);
        Scene scene = new Scene(root);
        Stage stage=new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(scene);
        ClientController controller = loader.<ClientController>getController();
        controller.initData(savedUsername);
        stage.show();
    }
}
