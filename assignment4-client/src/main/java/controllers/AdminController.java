package controllers;

import javafx.application.Application;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import services.AdminService;
import services.LoginService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class AdminController {
   @FXML
    private TextField sender;
   @FXML
    private TextField receiver;
   @FXML
    private TextField packageName;
   @FXML
    private TextField packageDescription;
   @FXML
    private TextField senderCity;
   @FXML
    private TextField destinationCity;
   @FXML
    private TextField trackingId;
   @FXML
    private TextField deleteId;
   @FXML
    private TextField updatingId;
   @FXML
    private TextField updatedCity;
   @FXML
    private TextField updateTime;

   private AdminService adminService;

public AdminController() throws MalformedURLException {
    URL url = new URL("http://localhost:8080/AdminService?wsdl");
    QName qName = new QName("http://sib/","AdminBeanService");
    Service service = Service.create(url,qName);
    qName = new QName("http://sib/","AdminBeanPort");
    this.adminService = service.getPort(qName, AdminService.class);
}
   public void createPackage(){

       adminService.addPackage(sender.getText(),receiver.getText(),packageName.getText(),packageDescription.getText(),senderCity.getText(),destinationCity.getText(),false);
       System.out.println("dada");

   }

   public void addPackageForTracking(){
        adminService.registerPackageForTracking(Integer.parseInt(trackingId.getText()));
   }

   public void deletePackage(){
        adminService.deletePackage(Integer.parseInt(deleteId.getText()));
   }

   public void packageStatusUpdate() throws ParseException {
       DateFormat df = new SimpleDateFormat("dd MM yyyy hh:mm");
       Date date = df.parse(updateTime.getText());
       GregorianCalendar calendar = new GregorianCalendar();
       calendar.setTime(date);

       adminService.packageStatusUpdating(Integer.parseInt(updatingId.getText()),updatedCity.getText(),calendar);

   }

}
