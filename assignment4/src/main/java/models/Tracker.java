package models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.GregorianCalendar;

@Entity
@Table(name = "tracks")
public class Tracker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    @Column
    private String city;
    @Column
    private GregorianCalendar time;

    @ManyToOne(targetEntity = Package.class)
    @JoinColumn(name = "package")
    private Package pack;



    public Tracker(String city, GregorianCalendar time,Package pack) {
        this.city = city;
        this.time = time;
        this.pack = pack;
    }
    public Tracker(){

    }
    public int getId() {
        return id;
    }

    public Package getPack() {
        return pack;
    }

    public void setPack(Package pack) {
        this.pack = pack;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public GregorianCalendar getTime() {
        return time;
    }

    public void setTime(GregorianCalendar time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Tracker{" +
                "id=" + id +
                ", city='" + city + '\'' +
                ", time=" + time +
                ", pack=" + pack +
                '}'+'\n';
    }
}
