package dao;

import models.Package;
import models.Tracker;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.Query;
import java.util.List;

public class TrackerDAO {
    private final StandardServiceRegistry registry = (new StandardServiceRegistryBuilder()).configure().build();
    private SessionFactory sessionFactory;
    public TrackerDAO(){
        try {
            this.sessionFactory = (new MetadataSources(this.registry)).buildMetadata().buildSessionFactory();
        } catch (Exception var2) {
            StandardServiceRegistryBuilder.destroy(this.registry);
            System.err.println("Error in hibernate: ");
            var2.printStackTrace();
        }
    }
@SuppressWarnings("Duplicates")
    public void createTrack(Tracker tracker){
        Session session=sessionFactory.openSession();
        Transaction tx=null;
        try{
            tx=session.beginTransaction();
            session.save(tracker);
            tx.commit();
        }
        catch (HibernateException ex){
            if(tx!=null){
                tx.rollback();
            }
        }
        finally {
            session.close();
        }
    }

    public List<Tracker> viewPackageStatus(Package pack){
        Session session=sessionFactory.openSession();
        List<Tracker> tracks=null;
        Transaction tx=null;

        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM Tracker WHERE pack =: pack");
            query.setParameter("pack",pack);
            tracks= query.getResultList();
            tx.commit();

        }
        catch(HibernateException ex){
            if(tx != null){
                tx.rollback();
            }

        }
        finally {
            session.close();
        }

        return tracks !=null && !tracks.isEmpty() ? tracks: null;
    }

}
