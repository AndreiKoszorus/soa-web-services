package dao;


import models.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.Query;
import java.util.List;

public class UserDAO {
    private final StandardServiceRegistry registry = (new StandardServiceRegistryBuilder()).configure().build();
    private SessionFactory sessionFactory;

    public UserDAO(){
        try {
            this.sessionFactory = (new MetadataSources(this.registry)).buildMetadata().buildSessionFactory();
        } catch (Exception var2) {
            StandardServiceRegistryBuilder.destroy(this.registry);
            System.err.println("Error in hibernate: ");
            var2.printStackTrace();
        }
    }
    @SuppressWarnings("Duplicates")
    public void createUser(User user){
        Session session=sessionFactory.openSession();
        Transaction tx=null;
        try{
            tx=session.beginTransaction();
            session.save(user);
            tx.commit();
        }
        catch (HibernateException ex){
            if(tx!=null){
                tx.rollback();
            }
        }
        finally {
            session.close();
        }
    }


    public User getUserByUsername(String username){
        Session session=sessionFactory.openSession();
        List<User> users=null;
        Transaction tx=null;

        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username =: username");
            query.setParameter("username",username);
            users= query.getResultList();
            tx.commit();

        }
        catch(HibernateException ex){
            if(tx != null){
                tx.rollback();
            }

        }
        finally {
            session.close();
        }

        return users !=null && !users.isEmpty() ? users.get(0): null;
    }
}
