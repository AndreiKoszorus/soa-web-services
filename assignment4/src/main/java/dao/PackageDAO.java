package dao;

import models.Package;
import models.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.Query;
import java.util.List;

public class PackageDAO {
    private final StandardServiceRegistry registry = (new StandardServiceRegistryBuilder()).configure().build();
    private SessionFactory sessionFactory;

    public PackageDAO(){
        try {
            this.sessionFactory = (new MetadataSources(this.registry)).buildMetadata().buildSessionFactory();
        } catch (Exception var2) {
            StandardServiceRegistryBuilder.destroy(this.registry);
            System.err.println("Error in hibernate: ");
            var2.printStackTrace();
        }
    }
@SuppressWarnings("Duplicates")
    public void createPackage(Package pack){
        Session session=sessionFactory.openSession();
        Transaction tx=null;
        try{
            tx=session.beginTransaction();
            session.save(pack);
            tx.commit();
        }
        catch (HibernateException ex){
            if(tx!=null){
                tx.rollback();
            }
        }
        finally {
            session.close();
        }
    }

    public Package getPackageById(int id){
        Session session=sessionFactory.openSession();
        List<Package> packages=null;
        Transaction tx=null;

        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE id =: id");
            query.setParameter("id",id);
            packages= query.getResultList();
            tx.commit();

        }
        catch(HibernateException ex){
            if(tx != null){
                tx.rollback();
            }

        }
        finally {
            session.close();
        }

        return packages !=null && !packages.isEmpty() ? packages.get(0): null;
    }

    public void updatePackage(Package pack) {

        Session session=sessionFactory.openSession();
        Transaction tx=null;
        try{
            tx=session.beginTransaction();
            session.update(pack);
            tx.commit();
        }
        catch (HibernateException ex){
            if(tx!=null){
                tx.rollback();
            }
        }
        finally {
            session.close();
        }
    }

    public void deletePackageById(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Package> packs= null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Package where id = :id");
            query.setParameter("id", id);
            packs = query.getResultList();
            Package deletedFlight = packs.get(0);
            session.delete(deletedFlight);
            tx.commit();

        }
        catch (HibernateException ex){
            if(tx != null){
                tx.rollback();
            }
        }
        finally {
            session.close();
        }

    }

    public List<Package> getUserPackages(User user){
        Session session=sessionFactory.openSession();
        List<Package> packages=null;
        Transaction tx=null;

        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE sender =: user");
            query.setParameter("user",user);
            packages= query.getResultList();
            tx.commit();

        }
        catch(HibernateException ex){
            if(tx != null){
                tx.rollback();
            }

        }
        finally {
            session.close();
        }

        return packages !=null && !packages.isEmpty() ? packages: null;
    }

    public List<Package> getUserPackageByName(User user, String name){
        Session session=sessionFactory.openSession();
        List<Package> packages=null;
        Transaction tx=null;

        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM Package WHERE sender =: user AND name =: name");
            query.setParameter("user",user);
            query.setParameter("name",name);
            packages= query.getResultList();
            tx.commit();

        }
        catch(HibernateException ex){
            if(tx != null){
                tx.rollback();
            }

        }
        finally {
            session.close();
        }

        return packages !=null && !packages.isEmpty() ? packages: null;
    }


    }

