package services;

import models.Package;
import models.Tracker;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.util.GregorianCalendar;

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL, parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface AdminService {

    @WebMethod
    void addPackage(String senderName,String receiverName,String name,String description,String senderCity,String destinationCityt,boolean tracking);
    @WebMethod
    void deletePackage(int id);
    @WebMethod
    void registerPackageForTracking(int id);
    @WebMethod
    void packageStatusUpdating(int id, String location, GregorianCalendar time);
}
