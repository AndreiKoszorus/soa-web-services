package services;


import models.Package;
import models.Tracker;
import models.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ClientService {

    @WebMethod
    List<Package> listPackages(String username);
    @WebMethod
    List<Package> searchPackages(String username, String packageName);
    @WebMethod
    List<Tracker>  packageStatus(int id,String username );
}
