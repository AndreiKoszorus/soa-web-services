package services;

import models.User;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface LoginService {

    @WebMethod
    int checkLogin(String username,String password);

    @WebMethod
    boolean isAdmin(String username);
}
