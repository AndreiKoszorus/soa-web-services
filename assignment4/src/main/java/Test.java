import dao.PackageDAO;
import dao.UserDAO;
import models.Package;
import models.User;
import sib.AdminBean;
import sib.ClientBean;
import sib.LoginBean;

import javax.xml.ws.Endpoint;
import java.util.List;

public class Test {

    public static void main(String[] args){
        User user = new User("andreikosz","andrei123",true);
        User user1 = new User("alex.alex","alex123",false);

        UserDAO userDAO = new UserDAO();
        PackageDAO packageDAO = new PackageDAO();

        //userDAO.createUser(user);
        //userDAO.createUser(user1);
       // User dbUser = userDAO.getUserByUsername("andreikosz");
        //User dbUser1 = userDAO.getUserByUsername("alex.alex");
        //Package pack = new Package(dbUser,dbUser1,"super pack","fragile","bucuresti","cluj-napoca",false);
        //packageDAO.createPackage(pack);
       //List<Package> pack2 = packageDAO.getUserPackageByName(dbUser,"super pack");
       //System.out.println(pack2.get(0).getName());
        //List<Package> getPackage = packageDAO.getUserPackages(dbUser1);

        Endpoint.publish("http://localhost:8080/AdminService",new AdminBean());
        Endpoint.publish("http://localhost:8080/ClientService",new ClientBean());
        Endpoint.publish("http://localhost:8080/LoginService",new LoginBean());

    }
}
