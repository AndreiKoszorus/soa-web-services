
package package1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the package1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ListPackagesResponse_QNAME = new QName("http://services/", "listPackagesResponse");
    private final static QName _PackageStatusResponse_QNAME = new QName("http://services/", "packageStatusResponse");
    private final static QName _SearchPackages_QNAME = new QName("http://services/", "searchPackages");
    private final static QName _PackageStatus_QNAME = new QName("http://services/", "packageStatus");
    private final static QName _ListPackages_QNAME = new QName("http://services/", "listPackages");
    private final static QName _SearchPackagesResponse_QNAME = new QName("http://services/", "searchPackagesResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: package1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListPackages }
     * 
     */
    public ListPackages createListPackages() {
        return new ListPackages();
    }

    /**
     * Create an instance of {@link SearchPackagesResponse }
     * 
     */
    public SearchPackagesResponse createSearchPackagesResponse() {
        return new SearchPackagesResponse();
    }

    /**
     * Create an instance of {@link PackageStatusResponse }
     * 
     */
    public PackageStatusResponse createPackageStatusResponse() {
        return new PackageStatusResponse();
    }

    /**
     * Create an instance of {@link ListPackagesResponse }
     * 
     */
    public ListPackagesResponse createListPackagesResponse() {
        return new ListPackagesResponse();
    }

    /**
     * Create an instance of {@link SearchPackages }
     * 
     */
    public SearchPackages createSearchPackages() {
        return new SearchPackages();
    }

    /**
     * Create an instance of {@link PackageStatus }
     * 
     */
    public PackageStatus createPackageStatus() {
        return new PackageStatus();
    }

    /**
     * Create an instance of {@link Package }
     * 
     */
    public Package createPackage() {
        return new Package();
    }

    /**
     * Create an instance of {@link Tracker }
     * 
     */
    public Tracker createTracker() {
        return new Tracker();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListPackagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "listPackagesResponse")
    public JAXBElement<ListPackagesResponse> createListPackagesResponse(ListPackagesResponse value) {
        return new JAXBElement<ListPackagesResponse>(_ListPackagesResponse_QNAME, ListPackagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackageStatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "packageStatusResponse")
    public JAXBElement<PackageStatusResponse> createPackageStatusResponse(PackageStatusResponse value) {
        return new JAXBElement<PackageStatusResponse>(_PackageStatusResponse_QNAME, PackageStatusResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchPackages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "searchPackages")
    public JAXBElement<SearchPackages> createSearchPackages(SearchPackages value) {
        return new JAXBElement<SearchPackages>(_SearchPackages_QNAME, SearchPackages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackageStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "packageStatus")
    public JAXBElement<PackageStatus> createPackageStatus(PackageStatus value) {
        return new JAXBElement<PackageStatus>(_PackageStatus_QNAME, PackageStatus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListPackages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "listPackages")
    public JAXBElement<ListPackages> createListPackages(ListPackages value) {
        return new JAXBElement<ListPackages>(_ListPackages_QNAME, ListPackages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchPackagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "searchPackagesResponse")
    public JAXBElement<SearchPackagesResponse> createSearchPackagesResponse(SearchPackagesResponse value) {
        return new JAXBElement<SearchPackagesResponse>(_SearchPackagesResponse_QNAME, SearchPackagesResponse.class, null, value);
    }

}
