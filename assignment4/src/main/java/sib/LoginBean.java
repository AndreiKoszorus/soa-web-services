package sib;

import dao.UserDAO;
import models.User;
import services.LoginService;

import javax.jws.WebService;

@WebService(endpointInterface = "services.LoginService")
public class LoginBean implements LoginService {

    private UserDAO userDAO;

    public LoginBean(){
        userDAO = new UserDAO();
    }

    public int checkLogin(String username,String password) {
        User user = userDAO.getUserByUsername(username);

        if(user !=null){
            if(user.getPassword().equals(password)){
                return 0;
            }
            else
                return 1;
        }

        return 2;
    }

    public boolean isAdmin(String username) {
        User user = userDAO.getUserByUsername(username);

        return user.isRole();
    }


}
