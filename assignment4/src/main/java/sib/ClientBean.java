package sib;

import dao.PackageDAO;
import dao.TrackerDAO;
import dao.UserDAO;
import models.Package;
import models.Tracker;
import models.User;
import services.ClientService;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "services.ClientService")
public class ClientBean implements ClientService {

    private PackageDAO packageDAO;
    private TrackerDAO trackerDAO;
    private UserDAO userDAO;

    public ClientBean(){
        this.packageDAO = new PackageDAO();
        this.trackerDAO = new TrackerDAO();
        this.userDAO = new UserDAO();
    }

    public List<Package> listPackages(String username) {
        User user = userDAO.getUserByUsername(username);
        return packageDAO.getUserPackages(user);
    }

    public List<Package> searchPackages(String username, String packageName) {
        User user = userDAO.getUserByUsername(username);
        return packageDAO.getUserPackageByName(user,packageName);
    }

    public List<Tracker> packageStatus(int id,String username) {
        Package pack = packageDAO.getPackageById(id);
        if (pack != null && pack.isTracking() && username.equals(pack.getSender().getUsername())) {
            return trackerDAO.viewPackageStatus(pack);
        }

        return null;

    }
}
