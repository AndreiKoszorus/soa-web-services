package sib;


import dao.PackageDAO;
import dao.TrackerDAO;
import dao.UserDAO;
import models.Package;
import models.Tracker;
import models.User;
import services.AdminService;

import javax.jws.WebService;
import java.util.GregorianCalendar;

@WebService(endpointInterface = "services.AdminService")
public class AdminBean implements AdminService {

    private PackageDAO packageDAO;
    private TrackerDAO trackerDAO;
    private UserDAO userDAO;

    public AdminBean(){
        this.packageDAO = new PackageDAO();
        this.trackerDAO = new TrackerDAO();
        this.userDAO = new UserDAO();
    }

    public void addPackage(String senderName,String receiverName,String name,String description,String senderCity,String destinationCity,boolean tracking) {

        User sender = userDAO.getUserByUsername(senderName);
        User receiver = userDAO.getUserByUsername(receiverName);

        if(sender!=null && receiver!=null) {
            Package pack = new Package(sender, receiver, name, description, senderCity, destinationCity, tracking);
            packageDAO.createPackage(pack);
        }
    }


    public void deletePackage(int id) {
        packageDAO.deletePackageById(id);
    }

    public void registerPackageForTracking(int id) {

        Package pack = packageDAO.getPackageById(id);
        if(pack!=null){
            pack.setTracking(true);
            packageDAO.updatePackage(pack);
        }
    }

    public void packageStatusUpdating(int id, String location, GregorianCalendar time) {
        Package pack = packageDAO.getPackageById(id);
        if(pack!=null){
            Tracker tracker = new Tracker(location,time,pack);
            trackerDAO.createTrack(tracker);
        }
    }


}
