
package package2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the package2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IsAdmin_QNAME = new QName("http://services/", "isAdmin");
    private final static QName _CheckLogin_QNAME = new QName("http://services/", "checkLogin");
    private final static QName _CheckLoginResponse_QNAME = new QName("http://services/", "checkLoginResponse");
    private final static QName _IsAdminResponse_QNAME = new QName("http://services/", "isAdminResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: package2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link IsAdminResponse }
     * 
     */
    public IsAdminResponse createIsAdminResponse() {
        return new IsAdminResponse();
    }

    /**
     * Create an instance of {@link IsAdmin }
     * 
     */
    public IsAdmin createIsAdmin() {
        return new IsAdmin();
    }

    /**
     * Create an instance of {@link CheckLogin }
     * 
     */
    public CheckLogin createCheckLogin() {
        return new CheckLogin();
    }

    /**
     * Create an instance of {@link CheckLoginResponse }
     * 
     */
    public CheckLoginResponse createCheckLoginResponse() {
        return new CheckLoginResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsAdmin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "isAdmin")
    public JAXBElement<IsAdmin> createIsAdmin(IsAdmin value) {
        return new JAXBElement<IsAdmin>(_IsAdmin_QNAME, IsAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "checkLogin")
    public JAXBElement<CheckLogin> createCheckLogin(CheckLogin value) {
        return new JAXBElement<CheckLogin>(_CheckLogin_QNAME, CheckLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "checkLoginResponse")
    public JAXBElement<CheckLoginResponse> createCheckLoginResponse(CheckLoginResponse value) {
        return new JAXBElement<CheckLoginResponse>(_CheckLoginResponse_QNAME, CheckLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsAdminResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "isAdminResponse")
    public JAXBElement<IsAdminResponse> createIsAdminResponse(IsAdminResponse value) {
        return new JAXBElement<IsAdminResponse>(_IsAdminResponse_QNAME, IsAdminResponse.class, null, value);
    }

}
