
package mypackage;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mypackage package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeletePackage_QNAME = new QName("http://services/", "deletePackage");
    private final static QName _RegisterPackageForTracking_QNAME = new QName("http://services/", "registerPackageForTracking");
    private final static QName _RegisterPackageForTrackingResponse_QNAME = new QName("http://services/", "registerPackageForTrackingResponse");
    private final static QName _PackageStatusUpdatingResponse_QNAME = new QName("http://services/", "packageStatusUpdatingResponse");
    private final static QName _AddPackageResponse_QNAME = new QName("http://services/", "addPackageResponse");
    private final static QName _PackageStatusUpdating_QNAME = new QName("http://services/", "packageStatusUpdating");
    private final static QName _AddPackage_QNAME = new QName("http://services/", "addPackage");
    private final static QName _DeletePackageResponse_QNAME = new QName("http://services/", "deletePackageResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mypackage
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddPackageResponse }
     * 
     */
    public AddPackageResponse createAddPackageResponse() {
        return new AddPackageResponse();
    }

    /**
     * Create an instance of {@link PackageStatusUpdating }
     * 
     */
    public PackageStatusUpdating createPackageStatusUpdating() {
        return new PackageStatusUpdating();
    }

    /**
     * Create an instance of {@link PackageStatusUpdatingResponse }
     * 
     */
    public PackageStatusUpdatingResponse createPackageStatusUpdatingResponse() {
        return new PackageStatusUpdatingResponse();
    }

    /**
     * Create an instance of {@link DeletePackageResponse }
     * 
     */
    public DeletePackageResponse createDeletePackageResponse() {
        return new DeletePackageResponse();
    }

    /**
     * Create an instance of {@link AddPackage }
     * 
     */
    public AddPackage createAddPackage() {
        return new AddPackage();
    }

    /**
     * Create an instance of {@link DeletePackage }
     * 
     */
    public DeletePackage createDeletePackage() {
        return new DeletePackage();
    }

    /**
     * Create an instance of {@link RegisterPackageForTracking }
     * 
     */
    public RegisterPackageForTracking createRegisterPackageForTracking() {
        return new RegisterPackageForTracking();
    }

    /**
     * Create an instance of {@link RegisterPackageForTrackingResponse }
     * 
     */
    public RegisterPackageForTrackingResponse createRegisterPackageForTrackingResponse() {
        return new RegisterPackageForTrackingResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "deletePackage")
    public JAXBElement<DeletePackage> createDeletePackage(DeletePackage value) {
        return new JAXBElement<DeletePackage>(_DeletePackage_QNAME, DeletePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterPackageForTracking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "registerPackageForTracking")
    public JAXBElement<RegisterPackageForTracking> createRegisterPackageForTracking(RegisterPackageForTracking value) {
        return new JAXBElement<RegisterPackageForTracking>(_RegisterPackageForTracking_QNAME, RegisterPackageForTracking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterPackageForTrackingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "registerPackageForTrackingResponse")
    public JAXBElement<RegisterPackageForTrackingResponse> createRegisterPackageForTrackingResponse(RegisterPackageForTrackingResponse value) {
        return new JAXBElement<RegisterPackageForTrackingResponse>(_RegisterPackageForTrackingResponse_QNAME, RegisterPackageForTrackingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackageStatusUpdatingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "packageStatusUpdatingResponse")
    public JAXBElement<PackageStatusUpdatingResponse> createPackageStatusUpdatingResponse(PackageStatusUpdatingResponse value) {
        return new JAXBElement<PackageStatusUpdatingResponse>(_PackageStatusUpdatingResponse_QNAME, PackageStatusUpdatingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "addPackageResponse")
    public JAXBElement<AddPackageResponse> createAddPackageResponse(AddPackageResponse value) {
        return new JAXBElement<AddPackageResponse>(_AddPackageResponse_QNAME, AddPackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PackageStatusUpdating }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "packageStatusUpdating")
    public JAXBElement<PackageStatusUpdating> createPackageStatusUpdating(PackageStatusUpdating value) {
        return new JAXBElement<PackageStatusUpdating>(_PackageStatusUpdating_QNAME, PackageStatusUpdating.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "addPackage")
    public JAXBElement<AddPackage> createAddPackage(AddPackage value) {
        return new JAXBElement<AddPackage>(_AddPackage_QNAME, AddPackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services/", name = "deletePackageResponse")
    public JAXBElement<DeletePackageResponse> createDeletePackageResponse(DeletePackageResponse value) {
        return new JAXBElement<DeletePackageResponse>(_DeletePackageResponse_QNAME, DeletePackageResponse.class, null, value);
    }

}
